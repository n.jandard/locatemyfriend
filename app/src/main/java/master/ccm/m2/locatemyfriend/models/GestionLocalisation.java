package master.ccm.m2.locatemyfriend.models;

import android.Manifest;
import android.app.Activity;
import android.location.Location;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;

import org.imperiumlabs.geofirestore.GeoFirestore;

import master.ccm.m2.locatemyfriend.services.AuthService;
import master.ccm.m2.locatemyfriend.services.DbService;

public class GestionLocalisation {

    /**
     *
     */
    private final static int MES_PERMISSIONS_COARSE_FINE = 12;

    /**
     * point d'entré pour intéragir avec la localisation
     */
    private FusedLocationProviderClient fusedLocationClient;

    /**
     * gère les paramètres de localisation
     */
    private LocationRequest locationRequest = LocationRequest.create();

    /***
     * activité android
     */
    private Activity monActivity;

    /**
     *
     */
    private  LocationSettingsRequest.Builder builder;

    /***
     *
     */
    SettingsClient settingsClient;
    /**
     * callBack pour la localisation
     */
    private LocationCallback mLocationCallback;
    /**
     *constructeur
     * @param activity activité android
     */
    public GestionLocalisation(Activity activity) {
        this.monActivity=activity;
        this.fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.monActivity);
        this.locationRequest = LocationRequest.create();
        // Create LocationSettingsRequest object using location request
        this.builder = new LocationSettingsRequest.Builder();

    }

    /**
     * demande la permission d'accéder aux droits localisation
     */
    public void demanderPermission() {
        ActivityCompat.requestPermissions(this.monActivity,
                new String[]{
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                },
                MES_PERMISSIONS_COARSE_FINE);
    }

    // Trigger new location updates at interval
    public void startLocationUpdates() {

        mLocationCallback=new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                // do work here
                onLocationChanged(locationResult.getLastLocation());
            }
        };

        this.locationRequest.setInterval(10000);
        this.locationRequest.setFastestInterval(5000);
        this.locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        this.builder.addLocationRequest(locationRequest);
        LocationSettingsRequest locationSettingsRequest = this.builder.build();

        settingsClient = LocationServices.getSettingsClient(this.monActivity);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if(this.fusedLocationClient!=null){
            this.fusedLocationClient.requestLocationUpdates(this.locationRequest,mLocationCallback,Looper.myLooper());
        }

    }

    /***
     * met à jour la localisation du user connecté et sauvegarde en bdd
     * @param lastLocation objet localisation qui contient la localisation
     */
    private void onLocationChanged(Location lastLocation) {
        // New location has now been determined
        String msg = "Updated Location: " +
                Double.toString(lastLocation.getLatitude()) + "," +
                Double.toString(lastLocation.getLongitude());


        // You can now create a LatLng Object for use with maps
       // LatLng latLng = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());

        FirebaseUser user= AuthService.getInstance().getCurrentUser();

        /**
         * enregistre la position du user conneté
         */
        DbService.getInstance().get("users", user.getUid()).get()
            .addOnCompleteListener(task -> {
                if(task.isSuccessful()) {
                    CollectionReference collectionRef = FirebaseFirestore.getInstance().collection("users");
                    GeoFirestore geoFirestore = new GeoFirestore(collectionRef);
                    geoFirestore.setLocation( user.getUid(),new GeoPoint(lastLocation.getLatitude(),lastLocation.getLongitude()));

                    //Toast.makeText(this.monActivity, msg, Toast.LENGTH_SHORT).show();
                }
            });

    }

    /**
     * Retourne la dernière position connue
     *
     */
    public void  getLastLocation(){

        this.fusedLocationClient.getLastLocation()

                .addOnSuccessListener(this.monActivity, new OnSuccessListener<Location>() {

                    @Override
                    public void onSuccess(Location location) {
                        if(location!=null){
                            onLocationChanged(location);
                        }
                    }

                }).addOnFailureListener(new OnFailureListener() {

            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("MapActivity", "Error trying to get last GPS location");
                e.printStackTrace();
            }

        });

    }

    /***
     * arrete la localisation
     */
    public void stopLocationUpdates(){
        this.fusedLocationClient.removeLocationUpdates(mLocationCallback);
        Log.i("MapActivity", "Location updates removed");
    }
}
