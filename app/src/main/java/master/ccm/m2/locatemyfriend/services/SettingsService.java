package master.ccm.m2.locatemyfriend.services;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.Map;

public class SettingsService {
    private static final SettingsService instance = new SettingsService();

    private DbService dbService;

    private SettingsService() {
        dbService = DbService.getInstance();
    }

    public static SettingsService getInstance() {
        return instance;
    }

    public Task<Void> setPreferences(String userId, Map values) {
        return dbService.updateMultiple("settings", userId, values);
    }

    public Task<DocumentSnapshot> getPreferences(String userId) {
        return dbService.get("settings", userId).get();
    }
}
