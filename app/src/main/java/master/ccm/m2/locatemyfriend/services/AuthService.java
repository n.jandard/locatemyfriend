package master.ccm.m2.locatemyfriend.services;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class AuthService {
    private static final AuthService instance = new AuthService();

    private FirebaseAuth firebaseAuth;

    private AuthService() {
        firebaseAuth = FirebaseAuth.getInstance();
    }

    public static AuthService getInstance() {
        return instance;
    }

    /**
     * récupère l'id du user connecté
     * @return id user connecté
     */
    public FirebaseUser getCurrentUser() {
        return firebaseAuth.getCurrentUser();
    }

    /**
     *ajoute un user
     * @param mail
     * @param password
     * @return
     */
    public Task<AuthResult> registerUser(String mail, String password) {
        return firebaseAuth.createUserWithEmailAndPassword(mail, password);
    }

    /**
     * se connecte
     * @param mail
     * @param password
     * @return
     */
    public Task<AuthResult> signIn(String mail, String password) {
        return firebaseAuth.signInWithEmailAndPassword(mail, password);
    }

    /**
     * remet à jour un mdp
     * @param mail
     * @return
     */
    public Task<Void> resetPassword(String mail) {
        return firebaseAuth.sendPasswordResetEmail(mail);
    }

    /**
     * se déconnecte
     */
    public void logOut() {
        firebaseAuth.signOut();
    }


}
