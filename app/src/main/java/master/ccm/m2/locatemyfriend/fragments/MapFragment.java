package master.ccm.m2.locatemyfriend.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.GeoPoint;

import org.imperiumlabs.geofirestore.GeoFirestore;
import org.imperiumlabs.geofirestore.GeoQuery;
import org.imperiumlabs.geofirestore.listeners.GeoQueryDataEventListener;

import java.util.HashMap;

import master.ccm.m2.locatemyfriend.R;
import master.ccm.m2.locatemyfriend.models.Settings;
import master.ccm.m2.locatemyfriend.models.User;
import master.ccm.m2.locatemyfriend.services.AuthService;
import master.ccm.m2.locatemyfriend.services.DbService;
import master.ccm.m2.locatemyfriend.services.NotificationService;

public class MapFragment extends Fragment implements OnMapReadyCallback {
    private DbService dbService;

    private User currentUser;
    private Settings userSettings;
    private HashMap<String, Marker> markers;
    private String userId;
    private Activity activity;
    private Context appContext;
    private GeoQuery geoQuery;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_map, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Initialisation des services
        dbService = DbService.getInstance();

        //Initialisation des variables
        currentUser = new User();
        userSettings = new Settings();

        // Initialisation de la mapView
        MapView mapView = view.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

        markers = new HashMap<>();
        userId = AuthService.getInstance().getCurrentUser().getUid();
        activity = this.getActivity();
        appContext = activity.getApplicationContext();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        initUserValues(googleMap);
        dbService.get("settings", userId).addSnapshotListener((snapshot, e) -> {
            if (e != null) {
                Log.w("updateData", "Listen failed.", e);
                return;
            }

            if (snapshot != null && snapshot.exists()) {
                userSettings = snapshot.toObject(Settings.class);
            }
            else {
                Log.d("updateData", "Current data: null");
            }
        });
        dbService.get("users", userId).addSnapshotListener((snapshot, e) -> {
            if (e != null) {
                Log.w("updateData", "Listen failed.", e);
                return;
            }

            if (snapshot != null && snapshot.exists()) {
                // Récupère la localisation en temps réel de l'utilisateur courant
                currentUser = snapshot.toObject(User.class);
                LatLng userLocation = new LatLng(currentUser.getL().getLatitude(), currentUser.getL().getLongitude());

                BitmapDescriptor color = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE);
                addMarkerForUser(googleMap, snapshot, appContext.getString(R.string.myLocation), color);

                CameraPosition cameraPosition = new CameraPosition.Builder().target(userLocation)
                        .zoom(15).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                // Récupère tous les amis et ennemis dans le rayon en km
                if(currentUser.getL() != null && userSettings.getRange() != null && geoQuery != null) {
                    geoQuery.setLocation(currentUser.getL(), userSettings.getRange().doubleValue());
                    Log.d("update", ""+userSettings.getRange().doubleValue());
                }
            } else {
                Log.d("updateData", "Current data: null");
            }
        });
    }

    /**
     * Met à jour le marqueur d'un utilisateur
     *
     * @param documentSnapshot Utilisateur cible
     * @param googleMap Carte à afficher
     */
    private void updateRelationPosition(DocumentSnapshot documentSnapshot , GoogleMap googleMap) {
        boolean resultEnemies = false;
        boolean resultFriend = false ;
        if(currentUser.getEnemies()!=null){
             resultEnemies = currentUser.getEnemies().contains(documentSnapshot.getId());
        }
        if(currentUser.getFriends()!=null) {
             resultFriend = currentUser.getFriends().contains(documentSnapshot.getId());
        }

        if(resultEnemies) {
            BitmapDescriptor color = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
            addMarkerForUser(googleMap, documentSnapshot, appContext.getString(R.string.enemy), color);
        }

        if(resultFriend) {
            BitmapDescriptor color = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN);
            addMarkerForUser(googleMap, documentSnapshot, appContext.getString(R.string.friend), color);
        }
        Log.d("update", " ---------- Passage Fonction : "+documentSnapshot.getId());
    }

    /**
     * Enlève un marqueur pour l'utilisateur cible (lorsqu'il quitte le rayon)
     *
     * @param documentSnapshot Utilisateur cible
     */
    private void removeMarkerForUser(DocumentSnapshot documentSnapshot) {
        if(markers.get(documentSnapshot.getId()) != null && !documentSnapshot.getId().equals(userId)) {
            markers.get(documentSnapshot.getId()).remove();
        }
    }

    /**
     * Ajoute un marqueur pour l'utilisateur cible sur la carte
     *
     * @param gMap Carte à afficher
     * @param documentSnapshot Utilisateur concerné
     * @param text Label avec type de relation + nom et prénom
     * @param color Couleur du marqueur
     */
    private void addMarkerForUser(GoogleMap gMap, DocumentSnapshot documentSnapshot, String text, BitmapDescriptor color) {
        String userTargetId = documentSnapshot.getId();
        User userTarget = documentSnapshot.toObject(User.class);
        String title = "";

        // Si un marqueur existe déjà pour l'utilisateur concerné on retire l'ancien marqueur
        if(markers.get(userTargetId) != null) {
            markers.get(userTargetId).remove();
        }

        // Si le user "target" est le user actuel on n'affiche pas son nom
        if(userTargetId.equals(userId)) {
            title = text;
        }
        else {
            title = text + userTarget.getFirstname() + " " + userTarget.getLastname();
        }

        // On ajoute le nouveau marqueur mis à jour
        LatLng userTargetLocation = new LatLng(userTarget.getL().getLatitude(), userTarget.getL().getLongitude());
        Marker marker = gMap.addMarker(new MarkerOptions().position(userTargetLocation).title(title).icon(color));

        markers.put(userTargetId, marker);
    }

    /**
     * Créé la GeoQuery et lui ajoute un listener pour envoyer des notifications quand un
     * ami/ennemi est proche et pour mettre à jour les marqueurs de la carte en fonction de l'entrée,
     * de la sortie, ou du déplacement à l'intérieur du rayon
     *
     * @param googleMap Carte à afficher
     * @param user Informations de base de l'utilisateur pour créer la GeoQuery
     * @param settings Distance définie par l'utilisateur pour afficher ses amis/ennemis et recevoir
     *                 les notifications associées
     */
    private void createGeoQuery(GoogleMap googleMap, User user, Settings settings) {
        CollectionReference collectionRef = dbService.collection("users");
        GeoFirestore geoFirestore = new GeoFirestore(collectionRef);
        geoQuery = geoFirestore.queryAtLocation(user.getL(), settings.getRange().doubleValue());

        geoQuery.addGeoQueryDataEventListener(new GeoQueryDataEventListener() {
            @Override
            public void onGeoQueryReady() {
                Log.d("update", " query OK");
            }

            @Override
            public void onGeoQueryError(Exception e) {
                Log.d("update", e.toString());
            }

            /**
             * lorsque un user bouge dans le rayon mais ne le quitte pas met à jour sa position
             * seulement
             *
             * @param documentSnapshot Document concerné
             * @param geoPoint Localisation
             */
            @Override
            public void onDocumentMoved(DocumentSnapshot documentSnapshot, GeoPoint geoPoint) {
                Log.d("update", "onDocumentMoved "+ documentSnapshot +" : " + geoPoint.getLatitude() + " " + geoPoint.getLongitude());
                updateRelationPosition(documentSnapshot ,googleMap);
            }

            /**
             * Lorsqu'un utilisateur quitte le rayon de détection, on supprime son marqueur
             * de la carte
             *
             * @param documentSnapshot Document concerné
             */
            @Override
            public void onDocumentExited(DocumentSnapshot documentSnapshot) {
                removeMarkerForUser(documentSnapshot);
                Log.d("update", "onDocumentExited"+documentSnapshot+" ");
            }

            /**
             * Lorsqu'un utilisateur rentre dans le rayon de détection, on envoie une notification
             * sur l'application pour informer qu'un ami/ennemi est proche
             *
             * @param documentSnapshot Document concerné
             * @param geoPoint Localisation
             */
            @Override
            public void onDocumentEntered(DocumentSnapshot documentSnapshot, GeoPoint geoPoint) {
                User userTarget = documentSnapshot.toObject(User.class);

                // le nom complet du user
                String userFullname = userTarget.getFirstname() + " " + userTarget.getLastname();
                boolean resultEnemies = false;
                boolean resultFriend = false ;

                // si il possède des ennemis
                if(currentUser.getEnemies()!=null){
                    resultEnemies = currentUser.getEnemies().contains(documentSnapshot.getId());
                }
                // si il possède des amis
                if(currentUser.getFriends()!=null) {
                    resultFriend = currentUser.getFriends().contains(documentSnapshot.getId());
                }

                // Création de la notification si l'utilisateur est concerné et si l'option d'avoir
                // des notifications est activée
                if(resultEnemies && userSettings.getEnemyNotification()) {
                    NotificationService.getInstance().createNotif(appContext.getString(R.string.enemyNearTitle), appContext.getString(R.string.enemyNear, userFullname), activity);
                }

                if(resultFriend && userSettings.getFriendNotification()) {
                    NotificationService.getInstance().createNotif(appContext.getString(R.string.friendNearTitle), appContext.getString(R.string.friendNear, userFullname), activity);
                }

                Log.d("update", "entered : " +geoPoint.getLatitude() + " " + geoPoint.getLongitude());

                // Met à jour la position de l'utilisateur
                updateRelationPosition(documentSnapshot ,googleMap);

            }

            /**
             * Tous les utilisateurs qui ont eu un changement de donnée
             * Non utilisé, on ne veut pas mettre à jour un marqueur si la
             * donnée ne concerne pas le rayon
             *
             * @param documentSnapshot document concerné
             * @param geoPoint Localisation
             */
            @Override
            public void onDocumentChanged(DocumentSnapshot documentSnapshot, GeoPoint geoPoint) {

            }
        });
    }

    /**
     * Permet d'initialiser les valeurs de base de l'utilisateur à la création de la
     * Google Map
     *
     * @param googleMap Map à afficher
     */
    private void initUserValues(GoogleMap googleMap) {
        dbService.get("settings", userId).get().continueWith(continuation -> {
            Settings settings = continuation.getResult().toObject(Settings.class);
            return dbService.get("users", userId).get().addOnSuccessListener(documentSnapshot -> {
                User user = documentSnapshot.toObject(User.class);
                createGeoQuery(googleMap, user, settings);
            });
        });
    }
}
