package master.ccm.m2.locatemyfriend;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import master.ccm.m2.locatemyfriend.services.AuthService;

public class LoginActivity extends AppCompatActivity {

    private EditText mail, password;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Initialisation des éléments
        mail = findViewById(R.id.editTextLoginMail);
        password = findViewById(R.id.editTextLoginPassword);
    }

    @Override
    public void onStart() {
        super.onStart();

        /* On vérifie si un utilisateur est actuellement connecté, si c'est
         * le cas, on redirige directement vers l'application
         */
        if (AuthService.getInstance().getCurrentUser() != null) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
        }
    }

    /**
     * OnClickListener : Redirige vers l'activité d'inscription au clic
     *
     * @param view
     */
    public void switchToRegister(View view) {
        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
    }

    /**
     * OnClickListener : Redirige vers l'activité d'oubli de mot de passe
     * au clic
     *
     * @param view
     */
    public void forgotPassword(View view) {
        startActivity(new Intent(LoginActivity.this, ResetPasswordActivity.class));
    }

    /**
     * OnClickListener : Effectue une tentative de connexion à l'application, les
     * identifiants sont vérifiés par Firebase, si l'authentification est réussie
     * on redirige l'utilisateur vers l'application, si elle échoue on lui affiche
     * un message
     *
     * @param view
     */
    public void signIn(View view) {
        String mailValue = mail.getText().toString();
        String passwordValue = password.getText().toString();

        AuthService.getInstance().signIn(mailValue, passwordValue)
            .addOnCompleteListener(task -> {
                if(!task.isSuccessful()) {
                    Toast.makeText(LoginActivity.this, getString(R.string.authFailed), Toast.LENGTH_LONG).show();
                }
                else {
                    LoginActivity.this.startActivity(new Intent(LoginActivity.this, MainActivity.class));
                }
            });
    }
}
