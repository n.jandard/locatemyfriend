package master.ccm.m2.locatemyfriend.services;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Map;

public class DbService {
    private static final DbService instance = new DbService();

    private FirebaseFirestore firebaseDb;

    private DbService() {
        firebaseDb = FirebaseFirestore.getInstance();
    }

    public static DbService getInstance() {
        return instance;
    }

    /**
     * ajoute une info dans la bdd
     * @param collection  la collection à inseret
     * @param document le document à inseret
     * @param data les données à jouter
     */
    public void add(String collection, String document, Object data) {
        firebaseDb.collection(collection).document(document).set(data);
    }

    /**
     * met  à jour une donnée
     * @param collection
     * @param document
     * @param field
     * @param value
     * @return
     */
    public Task<Void> update(String collection, String document, String field, Object value) {
        return firebaseDb.collection(collection).document(document).update(field, value);
    }

    public Task<Void> updateMultiple(String collection, String document, Map value) {
        return firebaseDb.collection(collection).document(document).update(value);
    }

    public Task<QuerySnapshot> search(String collectionPath, String field, String value) {
        return firebaseDb.collection(collectionPath).whereEqualTo(field, value).get();
    }

    /**
     * supprime les données
     */
    public void delete() {}

    /**
     * récure une donnée
     * @param collection
     * @param document
     * @return
     */
    public DocumentReference get(String collection, String document) {
        return firebaseDb.collection(collection).document(document);
    }

    /**
     * met à jour plusieurs infos
     * @param collection
     * @param field
     * @param value
     * @param limit
     * @return
     */
    public Task<QuerySnapshot> getMultiple(String collection, String field, Object value, long limit) {
        return firebaseDb.collection(collection)
                .whereEqualTo(field, value)
                .limit(limit)
                .get();
    }

    public CollectionReference collection(String collection) {
        return firebaseDb.collection(collection);
    }
}
