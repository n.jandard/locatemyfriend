package master.ccm.m2.locatemyfriend.models;

public class Settings {

    private Long range;
    private boolean friendNotification, enemyNotification;

    public Settings() {
        this.range = 20L;
        this.friendNotification = true;
        this.enemyNotification = true;
    }

    public Settings(Long range, boolean friendNotification, boolean enemyNotification) {
        this.range = range;
        this.friendNotification = friendNotification;
        this.enemyNotification = enemyNotification;
    }

    public Long getRange() {
        return range;
    }

    public boolean getFriendNotification() {
        return friendNotification;
    }

    public boolean getEnemyNotification() {
        return enemyNotification;
    }

    public void setRange(Long range) {
        this.range = range;
    }

    public void setFriendNotification(boolean friendNotification) {
        this.friendNotification = friendNotification;
    }

    public void setEnemyNotification(boolean enemyNotification) {
        this.enemyNotification = enemyNotification;
    }
}
