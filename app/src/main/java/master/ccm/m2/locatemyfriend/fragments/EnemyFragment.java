package master.ccm.m2.locatemyfriend.fragments;

import android.app.Dialog;
import android.content.Context;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import master.ccm.m2.locatemyfriend.R;
import master.ccm.m2.locatemyfriend.adapters.RelationAdapter;
import master.ccm.m2.locatemyfriend.models.User;
import master.ccm.m2.locatemyfriend.services.AuthService;
import master.ccm.m2.locatemyfriend.services.DbService;
import master.ccm.m2.locatemyfriend.services.RelationshipService;

public class EnemyFragment extends Fragment {
    private final static String relationType = "enemies";

    private ListView listView;
    private ArrayList<String> enemyListRaw, friendListRaw;
    private HashMap<String, User> enemyList;
    private EditText enemyMail;
    private DbService dbService;
    private AuthService authService;
    private RelationshipService relationshipService;
    private Context appContext;

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {



        return inflater.inflate(R.layout.fragment_enemy, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Initialisation des variables
        dbService = DbService.getInstance();
        authService = AuthService.getInstance();
        relationshipService = RelationshipService.getInstance();

        Dialog enemiesModal = new Dialog(getContext());
        enemiesModal.setContentView(R.layout.modal_enemy_add);

        enemyList = new HashMap<>();
        enemyListRaw = new ArrayList<>();
        friendListRaw = new ArrayList<>();
        listView = view.findViewById(R.id.enemyListView);
        enemyMail = enemiesModal.findViewById(R.id.enemy_mail_editText);
        appContext = getActivity().getApplicationContext();

        FloatingActionButton enemiesModalButton = view.findViewById(R.id.addEnemyFAB);
        Button enemiesSubmitButton = enemiesModal.findViewById(R.id.enemy_add_button);

        enemiesSubmitButton.setOnClickListener(v -> addEnemy());
        enemiesModalButton.setOnClickListener(v -> enemiesModal.show());

        getUserEnemyList();
    }

    /**
     * Permet d'ajouter un utilisateur en ami en entrant son adresse mail dans
     * la popup d'ajout
     *
     * - On ne peut pas ajouter l'utilisateur courant en ami
     * - On ne peut pas ajouter un utilisateur déjà ami avec nous
     * - Il faut que l'adresse mail entrée permette de retrouver un utilisateur existant
     *
     * Lors de l'ajout en ami d'un utilisateur une relation va être créé et l'utilisateur "cible"
     * aura l'utilisateur courant dans sa liste d'ami également
     */
    private void addEnemy() {
        String enemyMailValue = enemyMail.getText().toString();

        if (TextUtils.isEmpty(enemyMailValue)) {
            showMessage(R.string.errorEmptyFields);
            return;
        }

        if (enemyMailValue.equals(authService.getCurrentUser().getEmail())) {
            showMessage(R.string.errorCurrentUser);
            return;
        }

        dbService.search("users", "mail", enemyMailValue)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        if (task.getResult().isEmpty()) {
                            showMessage(R.string.errorNotFound);
                            return;
                        }
                        for(QueryDocumentSnapshot user : task.getResult()) {
                            if (enemyListRaw.contains(user.getId())) {
                                showMessage(R.string.errorAlreadyEnemies);
                                return;
                            }

                            if (friendListRaw.contains(user.getId())) {
                                showMessage(R.string.errorAlreadyFriends);
                                return;
                            }

                            relationshipService.addRelation(authService.getCurrentUser().getUid(), user.getId(), relationType)
                                    .addOnSuccessListener(v -> {
                                        showMessage(R.string.addEnemySuccess);
                                        getUserEnemyList();
                                    })
                                    .addOnFailureListener(v -> {
                                        showMessage(R.string.addEnemyFailure);
                                        getUserEnemyList();
                                    });
                        }
                    }
                    else {
                        showMessage(R.string.errorSearchFailed);
                    }
                });
    }

    /**
     * Permet de retrouver la liste des amis de l'utilisateur courant dans un 1er temps sous forme
     * d'ID pour identifier les utilisateurs amis puis sous la forme d'une liste affichable pour que
     * l'utilisateur puisse reconnaître ses amis
     */
    private void getUserEnemyList() {
        enemyList.clear();
        relationshipService.getListRaw(authService.getCurrentUser().getUid())
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()) {
                        User user = task.getResult().toObject(User.class);
                        enemyListRaw = user.getEnemies();
                        friendListRaw = user.getFriends();
                    }
                });

        relationshipService.getListToDisplay(authService.getCurrentUser().getUid(), relationType)
                .addOnSuccessListener(tasks -> {
                    for(int i = 0; i < enemyListRaw.size(); i++) {
                        addEnemyDataToList(enemyListRaw.get(i), tasks.get(i));
                    }
                });
    }

    /**
     * Crée une "HashMap" des amis de l'utilisateur pour que l'on puisse les traiter dans
     * l'adapter et initialise la "ListView" pour afficher les amis dans l'interface
     *
     * @param id ID de l'ami
     * @param element Objet User représentant l'ami
     */
    private void addEnemyDataToList(String id, Object element) {
        Task<DocumentSnapshot> taskInProgress = (Task<DocumentSnapshot>) element;

        taskInProgress.addOnCompleteListener(task -> {
            if(task.isSuccessful() && appContext != null) {
                enemyList.put(id, task.getResult().toObject(User.class));

                if(enemyList.size() == enemyListRaw.size()) {
                    initAdapter();
                }
            }
        });
    }

    /**
     * Utilitaire pour Toast
     *
     * @param msg R.id du message à afficher
     */
    private void showMessage(int msg) {
        Toast.makeText(appContext,
                getResources().getString(msg),
                Toast.LENGTH_SHORT).show();
    }

    /**
     * Permet d'initialiser l'adapter et le transmettre à la "listView"
     */
    private void initAdapter() {
        RelationAdapter adapter = new RelationAdapter(
                appContext, enemyList, relationType);

        adapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                getUserEnemyList();
            }
        });

        listView.setAdapter(adapter);
    }
}