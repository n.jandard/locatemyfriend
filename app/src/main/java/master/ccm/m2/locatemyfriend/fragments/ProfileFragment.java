package master.ccm.m2.locatemyfriend.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import master.ccm.m2.locatemyfriend.MainActivity;
import master.ccm.m2.locatemyfriend.R;
import master.ccm.m2.locatemyfriend.models.User;
import master.ccm.m2.locatemyfriend.services.AuthService;
import master.ccm.m2.locatemyfriend.services.DbService;

public class ProfileFragment extends Fragment {
    private Context appContext;
    private DbService dbService;
    private AuthService authService;
    private TextView userName, userLocation, userMail;
    private EditText firstnameEdit, lastnameEdit;
    private User currentUser;

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Initialisation des services
        dbService = DbService.getInstance();
        authService = AuthService.getInstance();
        appContext = getActivity().getApplicationContext();

        // Initialisation des composants
        userName = view.findViewById(R.id.profile_name);
        userMail = view.findViewById(R.id.profile_mail);
        userLocation = view.findViewById(R.id.profile_location);

        Dialog userModal = new Dialog(getContext());
        userModal.setContentView(R.layout.modal_user_modify);

        // Initialisation des éléments de la modal
        firstnameEdit = userModal.findViewById(R.id.user_firstname_editText);
        lastnameEdit = userModal.findViewById(R.id.user_lastname_editText);
        Button editButton = view.findViewById(R.id.profile_modify);
        Button modalSubmitButton = userModal.findViewById(R.id.user_edit_button);

        // Association des listeners
        editButton.setOnClickListener(v -> userModal.show());
        modalSubmitButton.setOnClickListener(v -> modifyUser());

        setUserInformations();
    }

    /**
     * Applique sur les éléments de la vue les éléments de la base de données
     */
    private void setUserInformations() {
        dbService.get("users", authService.getCurrentUser().getUid()).get()
            .addOnCompleteListener(task -> {
                if(task.isSuccessful() && appContext != null) {
                    currentUser = task.getResult().toObject(User.class);
                    String userNameString = currentUser.getFirstname() + " " + currentUser.getLastname();
                    String userLocationString = currentUser.getL().getLatitude() + ", " + currentUser.getL().getLongitude();

                    userName.setText(userNameString);
                    userMail.setText(currentUser.getMail());
                    userLocation.setText(userLocationString);
                }
        });
    }

    /**
     * OnClickListener : Modifie les informations de l'utilisateur par rapport au formulaire
     * rempli dans la modal
     */
    private void modifyUser() {
        Map<String, Object> updatedValues = new HashMap<>();
        String userId = authService.getCurrentUser().getUid();
        updatedValues.put("firstname", firstnameEdit.getText().toString());
        updatedValues.put("lastname", lastnameEdit.getText().toString());

        dbService.updateMultiple("users", userId, updatedValues)
            .addOnSuccessListener(task -> {
                showMessage(R.string.editProfileSuccess);
                setUserInformations();
                MainActivity activity = (MainActivity) getActivity();
                activity.setToolbarSubtitle(authService.getCurrentUser().getUid());
            })
            .addOnFailureListener(task -> {
                showMessage(R.string.editProfileFailure);
            });
    }

    /**
     * Utilitaire pour créer un Toast plus rapidement
     *
     * @param msg ID du message de R.string
     */
    private void showMessage(int msg) {
        Toast.makeText(appContext,
                getResources().getString(msg),
                Toast.LENGTH_SHORT).show();
    }

}
