package master.ccm.m2.locatemyfriend;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import master.ccm.m2.locatemyfriend.fragments.EnemyFragment;
import master.ccm.m2.locatemyfriend.fragments.FriendFragment;
import master.ccm.m2.locatemyfriend.fragments.MapFragment;
import master.ccm.m2.locatemyfriend.fragments.ProfileFragment;
import master.ccm.m2.locatemyfriend.fragments.SettingsFragment;
import master.ccm.m2.locatemyfriend.models.User;
import master.ccm.m2.locatemyfriend.services.AuthService;
import master.ccm.m2.locatemyfriend.services.DbService;

import master.ccm.m2.locatemyfriend.models.GestionLocalisation;

import static androidx.core.content.ContextCompat.getSystemService;

public class MainActivity extends AppCompatActivity {


    private GestionLocalisation gestionLocalisation;

    private Toolbar toolbar;
    private BottomNavigationView bottomNavView;
    private static final String CHANNEL_ID = "FRIEND_ENNEMI";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // demande les permissions pour la localisation et commence à récupérer la position
        gestionLocalisation= new GestionLocalisation(this);
        gestionLocalisation.demanderPermission();
        gestionLocalisation.startLocationUpdates();

        createNotificationChannel();

        //Si l'utilisateur n'est pas connecté on le renvoie sur l'activité de connexion
        if(AuthService.getInstance().getCurrentUser() != null) {
            toolbar = findViewById(R.id.mainToolbar);
            toolbar.setTitle(getString(R.string.app_name));
            setToolbarSubtitle(AuthService.getInstance().getCurrentUser().getUid());
        }
        else {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }

        loadFragment(new MapFragment());
        setBottomNavView();
    }

    /**
     * OnClickListener : Déconnecte l'utilisateur au moment de l'appui
     * sur le bouton
     *
     * @param view
     */
    public void userSignOut(View view) {
        AuthService.getInstance().logOut();
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
        gestionLocalisation.stopLocationUpdates();
        finish();
    }

    /**
     * Récupère le nom de l'utilisateur connecté pour afficher
     * sa session
     *
     * @param userId Identifiant de l'utilisateur connecté
     */
    public void setToolbarSubtitle(String userId) {
        DbService.getInstance().get("users", userId).get()
            .addOnCompleteListener(task -> {
                if(task.isSuccessful()) {
                    User userData = task.getResult().toObject(User.class);
                    MainActivity.this.toolbar
                            .setSubtitle(userData.getFirstname() + " " + userData.getLastname());
                }
            });
    }

    /**
     * Initialise la barre de navigation de l'activité avec le listener
     * quand on sélectionne un élément
     */
    private void setBottomNavView() {
        bottomNavView = findViewById(R.id.bottomBar);
        bottomNavView.setOnNavigationItemSelectedListener(menuItem -> {

            Fragment fragment = null;

            switch (menuItem.getItemId()) {
                case R.id.botNavMap:
                    fragment = new MapFragment();
                    break;

                case R.id.botNavFriend:
                    fragment = new FriendFragment();
                    break;

                case R.id.botNavEnemy:
                    fragment = new EnemyFragment();
                    break;

                case R.id.botNavProfile:
                    fragment = new ProfileFragment();
                    break;

                case R.id.botNavSettings:
                    fragment = new SettingsFragment();
                    break;
            }

            return loadFragment(fragment);
        });
    }

    /**
     * Permet d'afficher le fragment qui a été sélectionné grâce
     * au listener de la barre de navigation
     *
     * @param fragment Fragment à afficher
     * @return boolean Indication si le fragment a chargé correctement
     */
    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();

            return true;
        }
        return false;
    }

    /**
     * Crée un "channel" de notification (compatibilité avec l'API 26+)
     * Non obligatoire pour les API antérieures
     */
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,"FRIENDS&ENNEMIES", importance);
            channel.setDescription("Recevoir une notification lorsque un ami ou un ennemie sont proches");
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
        }
    }

}
