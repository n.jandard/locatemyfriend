package master.ccm.m2.locatemyfriend.services;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;

import java.util.ArrayList;
import java.util.List;

import master.ccm.m2.locatemyfriend.models.User;

public class RelationshipService {
    private static final RelationshipService instance = new RelationshipService();

    private DbService dbService;

    private RelationshipService() {
        dbService = DbService.getInstance();
    }

    public static RelationshipService getInstance() {
        return instance;
    }

    public Task<DocumentSnapshot> getListRaw(String userId) {
        return dbService.get("users", userId).get();
    }

    /**
     * Récupère la liste d'amis ou d'ennemis de l'utilisateur
     *
     * @param userId ID de l'utilisateur
     * @param type Type de liste que l'on veut récupérer (ami ou ennemi)
     * @return Une liste de Tasks à effectuer pour récupérer les infos des utilisateurs
     */
    public Task<List> getListToDisplay(String userId, String type) {
        return dbService.get("users", userId).get()
            .continueWith(continuation -> {
                List<Task> tasks = new ArrayList<>();
                ArrayList<String> list = new ArrayList<>();
                User userData = continuation.getResult().toObject(User.class);
                if(type.equals("friends")) {
                    list = userData.getFriends();
                }
                else if(type.equals("enemies")) {
                    list = userData.getEnemies();
                }
                list.forEach(friendId ->
                        tasks.add(dbService.get("users", friendId).get()));
                return tasks;
            });
    }

    /**
     * Ajoute l'utilisateur cible dans la liste ami ou ennemi de l'utilisateur source
     *
     * @param sourceUser ID de l'utilisateur source
     * @param targetUser ID de l'utilisateur cible
     * @param type Type de liste (ami ou ennemi)
     * @return Requête à valider avec un listener
     */
    public Task<Void> addRelation(String sourceUser, String targetUser, String type) {
        return dbService.update("users", sourceUser, type,
                FieldValue.arrayUnion(targetUser));
    }

    /**
     * Supprime l'utilisateur cible dans la liste ami ou ennemi de l'utilisateur source
     *
     * @param sourceUser ID de l'utilisateur source
     * @param targetUser ID de l'utilisateur cible
     * @param type Type de liste (ami ou ennemi)
     * @return Requête à valider avec un listener
     */
    public Task<Void> deleteRelation(String sourceUser, String targetUser, String type) {
        return dbService.update("users", sourceUser, type,
                FieldValue.arrayRemove(targetUser));
    }
}
