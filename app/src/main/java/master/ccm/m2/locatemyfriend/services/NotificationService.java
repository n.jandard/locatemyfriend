package master.ccm.m2.locatemyfriend.services;

import android.app.Activity;
import android.app.Notification;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import master.ccm.m2.locatemyfriend.R;

public class NotificationService  {

    private static final NotificationService instance = new NotificationService();
    private static final String CHANNEL_ID = "FRIEND_ENNEMI";

    private NotificationService() {

    }

    public static NotificationService getInstance() {
        return instance;
    }

    /**
     * Crée une notification et notifie l'utilisateur
     *
     * @param textTitle Le titre de la notification
     * @param textContent Le contenu de la notification
     * @param activity L'activité sur laquelle créer la notification
     */
    public  void createNotif(String textTitle , String textContent, Activity activity){
        NotificationCompat.Builder builder = new NotificationCompat.Builder(activity, CHANNEL_ID)
                .setContentTitle(textTitle)
                .setSmallIcon(R.drawable.user_svg)
                .setContentText(textContent)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                .setPriority(Notification.PRIORITY_MAX);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(activity);
        int id = (int) System.currentTimeMillis();
        notificationManager.notify(id, builder.build());


    }
}
