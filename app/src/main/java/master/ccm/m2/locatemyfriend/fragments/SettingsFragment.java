package master.ccm.m2.locatemyfriend.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import master.ccm.m2.locatemyfriend.R;
import master.ccm.m2.locatemyfriend.models.Settings;
import master.ccm.m2.locatemyfriend.services.AuthService;
import master.ccm.m2.locatemyfriend.services.SettingsService;

public class SettingsFragment extends Fragment {
    private SeekBar seekbarRange;
    private Switch switchFriend, switchEnemy;
    private TextView rangeTextView;
    private SettingsService settingsService;
    private AuthService authService;
    private Context appContext;

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialisation des services
        settingsService = SettingsService.getInstance();
        authService = AuthService.getInstance();
        appContext = getActivity().getApplicationContext();

        // Initialisation des composants
        seekbarRange = view.findViewById(R.id.seekBar);
        switchFriend = view.findViewById(R.id.switchFriend);
        switchEnemy = view.findViewById(R.id.switchEnemy);
        rangeTextView = view.findViewById(R.id.seekBar_view);
        Button buttonChanges = view.findViewById(R.id.buttonSettings_changes);

        initializeSettings();

        // Association des listeners aux composants
        buttonChanges.setOnClickListener(click -> updateSettings());
        seekbarRange.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                rangeTextView.setText(String.format(getString(R.string.range), i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    /**
     * Initialise les paramètres de l'utilisateur et les affiche
     */
    private void initializeSettings() {
        settingsService.getPreferences(authService.getCurrentUser().getUid())
            .addOnCompleteListener(task -> {
                if(task.isSuccessful() && appContext != null) {
                    Settings userSettings = task.getResult().toObject(Settings.class);

                    switchEnemy.setChecked(userSettings.getEnemyNotification());
                    switchFriend.setChecked(userSettings.getFriendNotification());
                    seekbarRange.setProgress(Math.toIntExact(userSettings.getRange()));
                    rangeTextView.setText(String.format(getString(R.string.range), userSettings.getRange()));
                }
            });
    }

    /**
     * Met à jour les paramètres en fonction des choix de l'utilisateur
     */
    private void updateSettings() {
        HashMap<String, Object> settingsValues = new HashMap<>();

        settingsValues.put("range", seekbarRange.getProgress());
        settingsValues.put("friendNotification", switchFriend.isChecked());
        settingsValues.put("enemyNotification", switchEnemy.isChecked());

        settingsService.setPreferences(authService.getCurrentUser().getUid(), settingsValues)
            .addOnSuccessListener(t -> {
                showMessage(R.string.changesSuccess);
            })
            .addOnFailureListener(t -> {
                showMessage(R.string.changesFailed);
            });
    }

    /**
     * Utilitaire pour créer un Toast plus rapidement
     *
     * @param msg ID du message de R.string
     */
    private void showMessage(int msg) {
        Toast.makeText(appContext,
                getResources().getString(msg),
                Toast.LENGTH_SHORT).show();
    }
}
