package master.ccm.m2.locatemyfriend.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import master.ccm.m2.locatemyfriend.R;
import master.ccm.m2.locatemyfriend.models.User;
import master.ccm.m2.locatemyfriend.services.AuthService;
import master.ccm.m2.locatemyfriend.services.RelationshipService;

public class RelationAdapter extends BaseAdapter {
    private Context appContext;
    private ArrayList<Map.Entry> userList;
    private RelationshipService relationshipService;
    private AuthService authService;
    private String relationType;

    public RelationAdapter(@NonNull Context context, Map<String, User> list, String type) {
        appContext = context;
        userList = new ArrayList<>();
        userList.addAll(list.entrySet());
        relationshipService = RelationshipService.getInstance();
        authService = AuthService.getInstance();
        relationType = type;
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Map.Entry<String, User> getItem(int position) {
        return userList.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;

        if(listItem == null) {
            listItem = LayoutInflater
                    .from(appContext)
                    .inflate(R.layout.list_view_item, parent, false);
        }

        String currentUserId = authService.getCurrentUser().getUid();
        String userId = getItem(position).getKey();

        User user = getItem(position).getValue();
        String userFullname = user.getFirstname() + " " + user.getLastname();

        TextView name = listItem.findViewById(R.id.list_item_name);
        TextView mail = listItem.findViewById(R.id.list_item_mail);
        ImageView icon = listItem.findViewById(R.id.list_item_delete);

        name.setText(userFullname);
        mail.setText(user.getMail());

        icon.setOnClickListener(view -> {
            relationshipService.deleteRelation(currentUserId, userId, relationType)
                    .addOnSuccessListener(v -> {
                        Toast.makeText(appContext, R.string.deleted, Toast.LENGTH_SHORT).show();
                        userList.remove(getItem(position));
                        RelationAdapter.this.notifyDataSetChanged();
                    });

            /* Si le type de relation est "ami" il faut également supprimer l'utilisateur courant
             * de la liste d'amis de l'utilisateur cible
             */
            if(relationType.equals("friends")) {
                relationshipService.deleteRelation(userId, currentUserId, relationType);
            }
        });

        return listItem;
    }
}