package master.ccm.m2.locatemyfriend.models;

import com.google.firebase.firestore.GeoPoint;

import java.util.ArrayList;

public class User {

    private String firstname, lastname, mail;
    private ArrayList<String> friends, enemies;
    private GeoPoint l;

    public User() {

    }

    public User(String firstname, String lastname, String mail) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.mail = mail;
        this.friends = new ArrayList<>();
        this.enemies = new ArrayList<>();
        this.l = new GeoPoint(0,0);
    }


    public User(String firstname, String lastname, String mail, double latitude, double longitude) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.mail = mail;
        this.l = new GeoPoint(latitude,longitude);
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getMail() {
        return mail;
    }

    public ArrayList<String> getFriends() {
        return friends;
    }

    public ArrayList<String> getEnemies() {
        return enemies;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public GeoPoint getL() {
        return l;
    }

    public void setLocation(GeoPoint location) {
        this.l = location;
    }

    public void setFriends(ArrayList<String> friends) {
        this.friends = friends;
    }

    public void setEnemies(ArrayList<String> enemies) {
        this.enemies = enemies;
    }

}
