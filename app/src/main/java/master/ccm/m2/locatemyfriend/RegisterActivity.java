package master.ccm.m2.locatemyfriend;

import android.content.Intent;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseUser;

import androidx.appcompat.app.AppCompatActivity;
import master.ccm.m2.locatemyfriend.models.Settings;
import master.ccm.m2.locatemyfriend.models.User;
import master.ccm.m2.locatemyfriend.services.AuthService;
import master.ccm.m2.locatemyfriend.services.DbService;

import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Objects;

public class RegisterActivity extends AppCompatActivity {

    private EditText mail, password, passwordConfirm, firstname, lastname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Initialisation des variables correspondant aux éléments de l'interface
        mail = findViewById(R.id.editTextRegisterMail);
        password = findViewById(R.id.editTextRegisterPassword);
        passwordConfirm = findViewById(R.id.editTextRegisterPasswordConfirm);
        firstname = findViewById(R.id.editTextRegisterFirstname);
        lastname = findViewById(R.id.editTextRegisterLastname);

        /* On teste si l'utilisateur n'est pas déjà connecté, si c'est le cas
        il est renvoyé à la page d'accueil */
        if(AuthService.getInstance().getCurrentUser() != null) {
            startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
        }
    }

    /**
     * OnClickListener : Permet d'inscrire un nouvel utilisateur
     * Crée un nouvel "FirebaseUser" (identifiants) et un nouvel "User" (profil)
     * @param view
     */
    public void registerUser(View view) {
        //Récupération des inputs utilisateur
        String mailValue = mail.getText().toString();
        String passwordValue = password.getText().toString();
        String passwordConfirmValue = passwordConfirm.getText().toString();
        String firstnameValue = firstname.getText().toString();
        String lastnameValue = lastname.getText().toString();

        //On vérifie si les champs sont remplis
        if(mailValue.isEmpty() || passwordValue.isEmpty() || firstnameValue.isEmpty() || lastnameValue.isEmpty()) {
            Toast.makeText(this, this.getString(R.string.errorEmptyFields), Toast.LENGTH_LONG).show();
            return;
        }

        //On vérifie si le mot de passe est assez long
        if(passwordValue.length() < 8) {
            Toast.makeText(this, this.getString(R.string.errorWeakPwd), Toast.LENGTH_LONG).show();
            return;
        }

        //On vérifie si le mot de passe et la confirmation sont identiques
        if(!passwordValue.equals(passwordConfirmValue)) {
            Toast.makeText(getApplicationContext(), this.getString(R.string.errorDiffPwd), Toast.LENGTH_SHORT).show();
            return;
        }

        //Si tout est bon, on crée un nouvel "FirebaseUser"
        AuthService.getInstance().registerUser(mailValue, passwordValue)
            .addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Toast.makeText(this, getString(R.string.registrationSuccess), Toast.LENGTH_LONG).show();
                    FirebaseUser fUser = Objects.requireNonNull(task.getResult()).getUser();

                    if(fUser != null) {
                        storeDetails(fUser, firstnameValue, lastnameValue);
                    }
                }
                else {
                    Toast.makeText(this, this.getString(R.string.errorRegistrationFailed), Toast.LENGTH_LONG).show();
                }
            });
    }

    /**
     * Enregistre les informations du "FirebaseUser" que l'on vient de créer
     * l'identifiant commun pour les reconnaitre est l'ID qui a été donné par
     * la "table" des FirebaseUser
     *
     * @param firebaseUser Utilisateur Firebase que l'on vient de créer
     * @param firstname Prénom de l'utilisateur
     * @param lastname Nom de l'utilisateur
     */
    private void storeDetails(FirebaseUser firebaseUser, String firstname, String lastname) {
        String mail = firebaseUser.getEmail();
        User newUser = new User(firstname,lastname, mail);
        DbService.getInstance().add("users", firebaseUser.getUid(), newUser);
        DbService.getInstance().add("settings", firebaseUser.getUid(), new Settings());
    }
}
