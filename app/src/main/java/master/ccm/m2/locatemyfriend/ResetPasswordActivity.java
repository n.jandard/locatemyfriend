package master.ccm.m2.locatemyfriend;

import androidx.appcompat.app.AppCompatActivity;
import master.ccm.m2.locatemyfriend.services.AuthService;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class ResetPasswordActivity extends AppCompatActivity {

    private EditText mail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        mail = findViewById(R.id.editTextResetMail);
    }

    /**
     * Permet d'envoyer un mail à l'utilisateur pour qu'il puisse réinitialiser
     * son mot de passe
     *
     * @param view
     */
    public void resetPassword(View view) {
        String userMail = mail.getText().toString();

        AuthService.getInstance().resetPassword(userMail).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Toast.makeText(ResetPasswordActivity.this,
                        getString(R.string.mailResetPwd), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(ResetPasswordActivity.this,
                        getString(R.string.failResetPwd), Toast.LENGTH_LONG).show();
            }
        });
    }
}
